--Update this list when adding files
local components_post = {
    "actionqueuer",
    "ambientsound",
    "amphibiouscreature",
    "areaaware",
    "butterflyspawner",
    "birdspawner",
    "builder",
    "burnable",
    "childspawner",
    "clock",
    "colourcube",
    "combat",
    "cookable",
    "crop",
    "cursable",
    "deployable",
    "drownable",
    "dryer",
    "dsp",
    "dynamicmusic",
    "eater",
    "edible",
    "equippable",
    "explosive",
    "fertilizer",
    "firedetector",
    "fishable",
    "fishingrod",
    "floater",
    "flotsamgenerator",
    "foodmemory",
    "frograin",
    "frostybreather",
    "fuel",
    "fueled",
    "follower",
    "growable",
    "health",
    "herdmember",
    "hounded",
    --"hunter",
    "inspectable",
    "inventory",
    "inventoryitem",
    "inventoryitemmoisture",
    "itemaffinity",
    "leader",
    "lighter",
    "locomotor",
    "lootdropper",
    "lureplantspawner",
    "minionspawner",
    "moisture",
    "oceancolor",
    "oldager",
	"penguinspawner",
    "perishable",
	"pickable",
    "placer",
    "plantregrowth",
    "playeractionpicker",
    "playercontroller",
    "playerspawner",
    "recallmark",
    "regrowthmanager",
    "repairable",
    "seasons",
    "shadowcreaturespawner",
    "shard_clock",
    "shard_seasons",
    "sheltered",
	"spell",
    "stackable",
    "stewer",
    "skinner",
	"tackler",
    "teamleader",
    "temperature",
    "thief",
    "trap",
    "uianim",
    "undertile",
    "weather",
    "weapon",
    "wildfires",
    "wisecracker",
    "witherable",
    "worldstate",
    "worldwind",
}

local prefabs_post = {
    "abigail",
    "appeasement_item",
    "antliontrinket",
    "ash",
    "balloonvest",
    "birdcage",
    "book_birds",
    "book_fish",
    "book_rain",
    "book_silviculture",
    "buff_workeffectiveness",
    "cactus",
    "campfire",
    "cave_entrance",
    "cookpot",
    "dirtpile",
    "eel",
    "fireflies",
    "firesuppressor",
    "fish",
    "float_fx",
    "gears",
    "gestalt",
    "glasscutter",
    "healthregenbuff",
    "heatrock",
    "houndwarning",
    "icebox",
    "inventoryitem_classified",
    "lantern",
    "lighter",
    "lightning",
	"lureplant",
    "oceanfish",
    "piratemonkeys",
    "marsh_bush",
    "meatrack",
    "meats",
    "megaflare",
    "merm",
    "mermhouse",
    "mermhouse_crafted",
    "mermking",
    --"minisign",
    "player_classified",
    "pocketwatch",
    "poison_immune",
    "portableblender",
    "portablecookpot",
    "portablespicer",
    "poop",
    "prototyper",
    "rainometer",
    "reskin_tool",
    "resurrectionstone",
    "sewing_tape",
    "shadowmeteor",
    "shadowskittish",
    "shadowwaxwell",
    "spicepack",
    "tallbird",
    "tentacle",
    "thunder_close",
    "thunder_far",
    "torch",
    "trinkets",
	"trophyscale_fish",
	"variants_ia",
    "warly",
    "warningshadow",
    "walls",
    "waxwell",
    "willow",
    "winterometer",
    "wobster",
    --"woby",
    "wolfgang",
    "woodie",
    "world",
    "wormwood_plant_fx",
    "wurt",
	"wx78_scanner",
	"wx78_scanner_item",
    "wx78",
}

local gustable_prefabs_post = {
    "bush",
    "grass",
    "palm",
    "sapling",
    "trees",
}

local scenarios_post = {
    "chestfunctions",
}

local stategraphs_post = {
    "bird",
    "commonstates",
    "shadowwaxwell",
    "tornado",
    "merm",
    "shadowcreature",
    "shadowwaxwell",
    "wilson",
    "wilson_client",
}

local brains_post = {
    "crabkingclawbrain",
    "mermbrain",
    "shadowwaxwellbrain",
}

local class_post = {
    "components/builder_replica",
    "components/combat_replica",
    "components/equippable_replica",
    "components/inventoryitem_replica",
    "screens/playerhud",
    "widgets/redux/craftingmenu_widget",
    "widgets/containerwidget",
    "widgets/healthbadge",
    "widgets/inventorybar",
    "widgets/itemtile",
    "widgets/mapwidget",
    "widgets/seasonclock",
    "widgets/widget",
}

modimport("postinit/sim")
modimport("postinit/any")
modimport("postinit/player")

for _,v in pairs(components_post) do
    modimport("postinit/components/"..v)
end

for _,v in pairs(prefabs_post) do
    modimport("postinit/prefabs/"..v)
end

for _,v in pairs(gustable_prefabs_post) do
    modimport("postinit/prefabs/gustable/"..v)
end

for _,v in pairs(scenarios_post) do
    modimport("postinit/scenarios/"..v)
end

for _,v in pairs(stategraphs_post) do
    modimport("postinit/stategraphs/SG"..v)
end

for _,v in pairs(brains_post) do
    modimport("postinit/brains/"..v)
end

for _,v in pairs(class_post) do
    --These contain a path already, e.g. v= "widgets/inventorybar"
    modimport("postinit/".. v)
end
