local assets =
{
	Asset("ANIM", "anim/grass_inwater.zip"),
	Asset("ANIM", "anim/grassgreen_build.zip"),
}

local function onregenfn(inst)
    inst.AnimState:PlayAnimation("grow")
    inst.AnimState:PushAnimation("idle", true)
	
	inst.Physics:SetCollides(true)
	inst.AnimState:SetLayer(LAYER_WORLD)
	inst.AnimState:SetSortOrder(0)
end

local function onpickedfn(inst, picker)
    inst.Physics:SetCollides(false)
    inst.SoundEmitter:PlaySound("dontstarve/wilson/pickup_reeds")
    
    inst.AnimState:PlayAnimation("picking")
    inst.AnimState:PushAnimation("picked", true)
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddMiniMapEntity()
    inst.entity:AddNetwork()
		
	MakeObstaclePhysics(inst, .25)

    inst.MiniMapEntity:SetIcon("grass_tropical.tex")

	inst.AnimState:SetBank("grass_inwater")
	inst.AnimState:SetBuild("grass_inwater")
    inst.AnimState:PlayAnimation("idle", true)

	inst:AddTag("plant")
    inst:AddTag("silviculture") -- for silviculture book

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst.AnimState:SetTime(math.random() * 2)
    local color = 0.75 + math.random() * 0.25
    inst.AnimState:SetMultColour(color, color, color, 1)

    inst:AddComponent("pickable")
    inst.components.pickable.picksound = "dontstarve/wilson/pickup_reeds"

    inst.components.pickable:SetUp("cutgrass", TUNING.GRASS_REGROW_TIME)
    inst.components.pickable.onregenfn = onregenfn
    inst.components.pickable.onpickedfn = onpickedfn

    inst:AddComponent("lootdropper")
    inst:AddComponent("inspectable")
		
    ---------------------

    MakeNoGrowInWinter(inst)
    MakeHauntableIgnite(inst)
		
	MakePickableBlowInWindGust(inst, TUNING.GRASS_WINDBLOWN_SPEED, TUNING.GRASS_WINDBLOWN_FALL_CHANCE)

    return inst
end

return Prefab("grass_water", fn, assets)