local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
----------------------------------------------------------------------------------------

local function fn(inst)
	inst:DoTaskInTime(0, function(inst)
        if inst:IsValid() and inst:HasTag("renewable") and IsInClimate(inst, "volcano") then
            inst:RemoveTag("renewable")
        end
    end)
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddPrefabPostInit("grass", fn)
