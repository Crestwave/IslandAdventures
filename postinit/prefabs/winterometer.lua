local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local _DoCheckTemp
local function DoCheckTemp(inst, ...)
	local _temperature = rawget(TheWorld.state, "temperature")
	if IsInIAClimate(inst) then
		TheWorld.state.temperature = TheWorld.state.islandtemperature
	end
	if _DoCheckTemp ~= nil then
		_DoCheckTemp(inst, ...)
	end
	if not inst:HasTag("burnt") and inst:HasTag("flooded") then
		inst.AnimState:SetPercent("meter", math.random())
	end
	TheWorld.state.temperature = _temperature
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddPrefabPostInit("winterometer", function(inst)
	inst:AddComponent("floodable")
	if TheWorld.ismastersim then
		inst.components.floodable:SetFX("shock_machines_fx",5)
		if not _DoCheckTemp then
			for i, v in ipairs(inst.event_listening["animover"][inst]) do
				if UpvalueHacker.GetUpvalue(v, "DoCheckTemp") then
					_DoCheckTemp = UpvalueHacker.GetUpvalue(v, "DoCheckTemp")
					UpvalueHacker.SetUpvalue(v, DoCheckTemp, "DoCheckTemp")
					break
				end
			end
		end
		--reset
		if inst.task ~= nil then
			inst.task:Cancel()
			inst.task = nil
		end
		inst:PushEvent("animover")
	end
end)

