local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

---------------------------------------------------------------------------------------------------------------------------------------------

local LIGHT = "LIGHT"
local MEDIUM = "MEDIUM"
local HEAVY = "HEAVY"

local addBlowInWind = {
	nubbin = MEDIUM,
	acorn = MEDIUM,
	balloons_empty = LIGHT,
	balloon = LIGHT,	
	balloonspeed = LIGHT,
	balloonparty = LIGHT,
	balloonvest = LIGHT,
	balloonhat = LIGHT,
	butterflywings = LIGHT,
	moonbutterflywings = LIGHT,
	charcoal = LIGHT,
	cutgrass = LIGHT,
	cutreeds = LIGHT,
	bird_egg = LIGHT,
	featherfan = LIGHT,
	feather_crow = LIGHT,
	feather_robin = LIGHT,
	feather_robin_winter = LIGHT,
	feather_canary = LIGHT,
	malbatross_feather = LIGHT,
	goose_feather = LIGHT,
	guano = MEDIUM,
	flint = HEAVY,
	froglegs = MEDIUM,
	gears = HEAVY,
	goldnugget = MEDIUM,
	moonrocknugget = HEAVY,
	moonglass = MEDIUM,
	moonglass_charged = MEDIUM,
	heatrock = HEAVY,
	rocks = HEAVY,
	saltrock = MEDIUM,
	marble = HEAVY,
	rock_avocado_fruit = HEAVY,
	ice = MEDIUM,
	cave_banana = LIGHT,
	fig = LIGHT,
	berries = LIGHT,
	berries_cooked = LIGHT,
	berries_juicy = LIGHT,
	berries_juicy_cooked = LIGHT,
	kelp = LIGHT,
	kelp_cooked = LIGHT,
	kelp_dried = LIGHT,
	livinglog = HEAVY,
	log = HEAVY,
	slurtle_shellpieces = LIGHT,
	lureplantbulb = HEAVY,
	batwing = MEDIUM,
	batwing_cooked = MEDIUM,
	driftwood_log = MEDIUM,
	drumstick = MEDIUM,
	drumstick_cooked = MEDIUM,
	trunk_summer = MEDIUM,
	trunk_winter = MEDIUM,
	trunk_cooked = MEDIUM,
	plantmeat = MEDIUM,
	plantmeat_cooked = MEDIUM,
	meat = MEDIUM,
	mussel = MEDIUM,
	mussel_cooked = MEDIUM,
    barnacle = MEDIUM,
	barnacle_cooked = MEDIUM,
	cookedmeat = MEDIUM,
	meat_dried = MEDIUM,
	smallmeat = LIGHT,
	cookedsmallmeat = LIGHT,
	smallmeat_dried = LIGHT,
	monstermeat = MEDIUM,
	cookedmonstermeat = MEDIUM,
	monstermeat_dried = MEDIUM,
    blue_cap = LIGHT,
    green_cap = LIGHT,
    red_cap = LIGHT,
    moon_cap = LIGHT,
    red_cap_cooked = LIGHT,
    blue_cap_cooked = LIGHT,
    green_cap_cooked = LIGHT,
    moon_cap_cooked = LIGHT,
	nightmarefuel = LIGHT,
	nitre = MEDIUM,
	papyrus = LIGHT,
	petals = LIGHT,
	petals_evil = LIGHT,
	pinecone = LIGHT,
	palmcone_scale = LIGHT,
    palmcone_seed = LIGHT,
	pigskin = MEDIUM,
	poop = MEDIUM,
	rottenegg = LIGHT,
	rope = LIGHT,
	seeds = LIGHT,
	silk = LIGHT,
	spidergland = MEDIUM,
	spoiled_food = MEDIUM,
	spoiled_fish_small = MEDIUM,
	spoiled_fish = MEDIUM,
	spoiled_fish_large = MEDIUM,
	stinger = LIGHT,
	torch = MEDIUM,
	transistor = MEDIUM,
	twigs = LIGHT,
	twiggy_nut = LIGHT,
	dug_marsh_bush = MEDIUM,
    dug_coffeebush = MEDIUM,
    dug_elephantcactus = MEDIUM,
    dug_berrybush = MEDIUM,
    dug_berrybush2 = MEDIUM,
    dug_berrybush_juicy = MEDIUM,
    dug_bananabush = MEDIUM,
    dug_monkeytail = MEDIUM,
    dug_sapling = MEDIUM,
    dug_sapling_moon = MEDIUM,
    dug_grass = MEDIUM,
    dug_bambootree = MEDIUM,
    dug_bush_vine = MEDIUM,
    dug_rock_avocado_bush = MEDIUM,
	carrot = LIGHT,
	carrot_seeds = LIGHT,
	corn = LIGHT,
	corn_seeds = LIGHT,
	pumpkin = MEDIUM,
	pumpkin_seeds = LIGHT,
	eggplant = MEDIUM,
	eggplant_seeds = LIGHT,
	durian = LIGHT,
	durian_seeds = LIGHT,
	pomegranate = LIGHT,
	pomegranate_seeds = LIGHT,
	dragonfruit = LIGHT,
	dragonfruit_seeds = LIGHT,
	watermelon = MEDIUM,
	watermelon_seeds = LIGHT,
	tomato = LIGHT,
	tomato_seeds = LIGHT,
	potato = LIGHT,
	potato_seeds = LIGHT,
	asparagus = LIGHT,
	asparagus_seeds = LIGHT,
	onion = LIGHT,
	onion_seeds = LIGHT,
	garlic = LIGHT,
	garlic_seeds = LIGHT,
	pepper = LIGHT,
	pepper_seeds = LIGHT,
	sweet_potato = LIGHT,
	sweet_potato_seeds = LIGHT,
}

local neveronwater = {
	walrus_camp = true,
	evergreen = true,
	pinecone_sapling = true,
	evergreen_sparse = true,
	lumpy_sapling = true,
	twiggytree = true,
	twiggy_nut_sapling = true,
	deciduoustree = true,
	acorn_sapling = true,
	livingtree_sapling = true,
	sapling = true,
	grass = true,
	berrybush = true,
	berrybush_juicy = true,
	flower = true,
	flower_evil = true,
	rock1 = true,
	rock2 = true,
	skeleton = true,
}
local function RemoveOnWater(inst)
	if IsOnWater(inst) then
		inst:Remove()
	end
end

---------------------------------------------------------------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
---------------------------------------------------------------------------------------------------------------------------------------------

IAENV.AddPrefabPostInitAny(function(inst)
    if inst and TheWorld.ismastersim then
        --if this list gets larger we can modify this a bit.
        if (inst.prefab == "wx78" or inst:HasTag("shadow") or inst:HasTag("chess") or inst:HasTag("wall") or inst:HasTag("poisonimmune")
		or inst:HasTag("mech") or inst:HasTag("brightmare") or inst:HasTag("hive") or inst:HasTag("ghost") or inst:HasTag("veggie")
		or inst:HasTag("balloon"))
    	and inst.poisonimmune ~= false then inst.poisonimmune = true end

        if inst.components and inst.components.combat and inst.components.health and not inst.poisonimmune and not inst.components.poisonable then
            if inst:HasTag("player") then
                MakePoisonableCharacter(inst, nil, nil, "player", 0, 0, 1)
                inst.components.poisonable.duration = TUNING.TOTAL_DAY_TIME * 3
                inst.components.poisonable.transfer_poison_on_attack = false
            else
                MakePoisonableCharacter(inst)
            end
        end
    	
    	if addBlowInWind[inst.prefab] then
    		MakeBlowInHurricane(inst, TUNING.WINDBLOWN_SCALE_MIN[addBlowInWind[inst.prefab]], TUNING.WINDBLOWN_SCALE_MAX[addBlowInWind[inst.prefab]])
    	end

    	if neveronwater[inst.prefab] then
    		inst:DoTaskInTime(0,RemoveOnWater)
    	end

		if (inst:HasTag("tentacle") or inst:HasTag("flup")) and not inst:HasTag("alert_wurt") then
			inst:AddTag("alert_wurt")
		end

		if inst.components.inventoryitem ~= nil then
			inst.components.inventoryitem:InitInvPhysics()
		end

        if inst:HasTag("SnowCovered") then
            if not inst.components.climatetracker then
    			inst:AddComponent("climatetracker")
    		end

            --objects that move between climates now properly update being snow covered.
            inst:ListenForEvent("climatechange", function(inst, data)
                if not IsInIAClimate(inst) and TheWorld.state.issnowcovered then
                    inst.AnimState:Show("snow")
                else
                    inst.AnimState:Hide("snow")
                end
            end)
        end

    end
end)
